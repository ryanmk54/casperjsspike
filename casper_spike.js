var casper = require('casper').create();

casper.start('https://www.ksl.com/public/member/signin?login_forward=%2F', function() {
    this.echo(this.getTitle());
    this.fillSelectors('form#dado_form_3', {
      'input[name="member[email]"]': 'ryanmk54@gmail.com',
      'input[name="member[password]"]': 'kslPass'
    }, true);
    this.click('input.continue');
    this.wait(5000, function() { 
      console.log("Done waiting 5 seconds");
      this.captureSelector('after_login.png', '#headBlock');
    });
});


casper.run();
